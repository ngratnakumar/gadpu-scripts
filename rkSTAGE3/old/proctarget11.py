import os
import spam
import socket
import sys
import glob
import subprocess
from datetime import datetime
from gadpudbapi.requests_api import getRequest, postRequest

hostname = socket.gethostname()
spam.set_aips_userid(11)


def __main__():
    # Setting the Process Start Date Time
    start_time = str(datetime.now())
    # Taking system's in/out to backup variable
    original_stdout = sys.stdout
    original_stderr = sys.stderr
    # Getting the unprocessed fits_id from the GADPU API
    fits_id = getRequest("getfitsid")
    fits_id = fits_id.content
    # If any fits exists, then proceed, else do nothing
    if fits_id:
        # Getting the unprocessed fits file details from the GADPU API
        fits_details = postRequest("getfitsdetails", fits_id)
        # Setting the fits_id to "processing" state GADPU API
        setStatus = postRequest("setProcessingStatus", fits_id)
        if setStatus:
            print "Updated the status to processing"
        # Gathering the fits_id's details and formatting them
        base_path = fits_details[1]
        pre_cali_dir = fits_details[2]
        uvfits_base_path = base_path + pre_cali_dir
        uvfits_file = fits_details[3]
        image_dir = fits_details[4]
        uvfits_full_path = uvfits_base_path + uvfits_file
        uvfits_full_path = uvfits_full_path
        # The Split Uvfits that is about to be processed
        print "Processing : " + uvfits_full_path
        uvfits_base_path = uvfits_base_path
        image_base_dir = base_path + image_dir
        image_base_dir = image_base_dir.replace("//", "/")
        # If process_target is success, the files should be moved to image_base_dir
        # /GARUDATA/IMAGING24/CYCLE24/PROJECT_CODE/FITS_IMAGES/
        print "Data will move to: " + image_base_dir
        # SPAM working directory in current GARUDA Node
        thread_dir = os.getcwd()
        # Changing directory to fits/
        os.chdir("fits/")
        # Opening logger process_target.log in append mode
        # This is not required because, the process_target() will have
        # its own logger, which will log AIPS commands in /datfil/
        # spam-SOURCE_NAME.log
        process_target_log = open('process_target.log', 'a+')
        process_target_log.write('\n\n\n******PROCESS TARGET STARTED******\n\n\n')
        process_target_log.write("--->  Start Time " + start_time)
        # Logging Compute Node Hostname
        process_target_log.write("\n Node name" + hostname + "\n")
        # Logging all Standard In/Output
        sys.stdout = process_target_log
        sys.stderr = process_target_log
        datfil_dir = thread_dir + "/datfil/"
        fits_dir = thread_dir + "/fits/"
        print "Copying " + uvfits_full_path + " to " + fits_dir
        os.system("cp " + uvfits_full_path + " " + fits_dir)
        # Starting spam.process_target(SPLIT_FITS_FILE)
        try:
            spam.process_target(uvfits_file, allow_selfcal_skip=True, add_freq_to_name=True)
            # If this process_target is success call
            # GADPU API setSuccessStatus for the current fits_id
            postRequest("setSuccessStatus", fits_id)
            print("spam.process_tagret " + uvfits_file)
        except Exception, e:
            process_target_log.write("Error: " + str(e))
            # If this process_target is a failure call
            # GADPU API setFailedStatus for the current fits_id
            postRequest("setfailedStatus", fits_id)
            print("Error: spam.process_tagret Failed " + uvfits_file)
            # Even the process is Success/Failed we remove
            # the Initially copied SPLIT_FITS_file, to save
            # disk space
        os.system('rm ' + uvfits_file)
        # recording the process end time to Log
        end_time = str(datetime.now())
        # Creating a new dir at BASE_DIR - FITS_IMAGES
        print "Make dir at " + image_base_dir
        os.system("mkdir -p " + image_base_dir)
        # Moving all the files from datfil/ to fits/
        # Moving all the processed files from fits/ to FITS_IMAGE@BASE_DIR
        print "moving back the processed files from " + fits_dir + " to " + image_base_dir
        # The below print statement is only for recording purpose,
        # actual removing the THREAD directory is done after the
        # STDIN/OUT controls are reverted.
        print "Removing the " + thread_dir + " from " + hostname
        process_target_log.write("End Time " + end_time)
        # Flushing the all processed out log to the log_file
        process_target_log.flush()
        # reverting stdin/out controls
        sys.stdout = original_stdout
        sys.stderr = original_stderr
        # Getting the list of datfil/spam_logs for summarize the process
        spam_log = glob.glob(datfil_dir + "spam*.log")
        # if spam_log is non-empty list, procced
        print spam_log
        if spam_log:
            # for every spam*.log file in the datfile file
            for each_spam_log in spam_log:
                original_stdout = sys.stdout
                original_stderr = sys.stderr
                failed = os.popen('grep "processing of field" ' + each_spam_log + ' | grep "failed" | wc -l').read()
                if int(failed.strip()) > 0:
                    postRequest("setfailedStatus", fits_id)
                print each_spam_log
                # getting summary of the log file
                summ_file = each_spam_log.replace(".log", ".summary")
                print summ_file
                summary_filename = open(summ_file, 'a+')
                # making the spam*.summary file and write the
                # summarize_spam_log output
                summary_filename.write('\n\n******SUMMARY LOG******\n\n')
                sys.stdout = summary_filename
                sys.stderr = summary_filename
                spam.summarize_spam_log(each_spam_log)
                sys.stdout = original_stdout
                sys.stderr = original_stderr
                summary_filename.flush()
        # Once the summary file is created inside the fits/
        # Move all the fits/ to FITS_IMAGE@BASE_DIR
        print "Moving datfil/ to fits/"
        # datfil_dir = datfil_dir + "*"
        # move_datfil_process = subprocess.Popen(['mv', datfil_dir, fits_dir])
        # move_datfil_process.communicate()

        # fits_dir = fits_dir + "*"
        # move_fits_process = subprocess.Popen(['mv', fits_dir, image_base_dir])
        # move_fits_process.communicate()
        movedata = os.system('mv ' + datfil_dir + '* ' + fits_dir)
        
        movefits = os.system("mv " + fits_dir + "* " + image_base_dir)
        # Changing the directory to /home/gadpu, inorder to delete the
        # current THREAD dir
        os.chdir('../../')
        print "Changed to " + os.getcwd()
        # Removing the current THREAD directory

        # delete_thread_process = subprocess.Popen(['rm', '-rf', thread_dir])
        # delete_thread_process.communicate()
        
        removethread = os.system('rm -rf ' + thread_dir)
        # exiting the SPAM process and cleaning the cache memory
        spam.exit()


if __name__ == '__main__':
    __main__()
