import spam
import time
import os, re
import glob
import trace
import sys
import time,datetime
import socket
from config import *

hostname=socket.gethostname()
spam.set_aips_userid(14)

failed_files = open('datfil/failed_files.log','a+')
succeeded_files = open('datfil/succeeded_files.log','a+')
succeeded_files.write('Successful final processing\n\n')
succeeded_files.flush()

failed_files.write('Failed files\n\n')
failed_files.flush()
directory_log_pre = open('directory_log_pre.txt','a+')
directory_log_post = open('directory_log_post.txt','a+')
os.system('pwd')

def main():
    #Copy all LTA files in valid obs dir to fits directory for processing
    for CURRENT_DIR in THREAD_FILES:
        #If valid.log present in directory, skip the directory else process
        if "valid.log" not in os.listdir(CURRENT_DIR+'/'):
            directory_log_pre.write(CURRENT_DIR + '\n')
            directory_log_pre.flush()
            pre(CURRENT_DIR)
            sys.exit()
        else:
            continue

#conversion and precalibration
def pre(CURRENT_DIR):
    os.system('touch ' + CURRENT_DIR +'/valid.log')
    os.chdir(CURRENT_DIR+'/')
    P_SPLIT_FILE_LINK = os.listdir('.')
    P_SPLIT_FILE_REAL = os.path.realpath(P_SPLIT_FILE_LINK[0])
    lta_list = P_SPLIT_FILE_REAL.split('/')[-2] #Name of the parent LTA file
    os.chdir(c_path +'/')
    print('cp ' + CURRENT_DIR + '/*RRLL*P*UVFITS' + ' fits/')
    os.system('cp ' + CURRENT_DIR + '/*RRLL*P*UVFITS' + ' fits/')

    added_precal = os.listdir('fits/') 

    directory_log_post.write(CURRENT_DIR + '\n')
    directory_log_post.flush()
    start = time.time()
    for precal_file in added_precal:
        post(CURRENT_DIR,precal_file,lta_list)
    end = time.time()

    os.chdir('fits/')
    timefile = open('time_process.txt','a+')
    timefile.write(str(datetime.timedelta(seconds=end-start)))
    timefile.flush()

    for added_precal_file in added_precal:
        if '.UVFITS' in added_precal_file:	
            os.system('mv ' + added_precal_file + ' ' + added_precal_file.replace('.UVFITS','') + '_' + lta_list + '.UVFITS')

    os.chdir('../')
    os.system('mkdir -p /GARUDATA/FITS/CYCLE_21_IMAGES/' + lta_list)
    os.system('rm -f fits/*.UVFITS')
    os.system('mv fits/*' + ' /GARUDATA/FITS/CYCLE_21_IMAGES/'+ lta_list)

#process target files
def post(CURRENT_DIR,precal_uvfits,lta):
    os.chdir('fits/')
    process_target_log = open('process_log.txt','a+')
    old_datfil_log = os.listdir('../datfil/')
    old_files = os.listdir('./')
    try:	
        spam.process_target(precal_uvfits)
        process_target_log.write(hostname+"\n"+"Successful final processing of "+str(precal_uvfits)+'\n')
        process_target_log.flush()
        succeeded_files.write(CURRENT_DIR + '/'+'\n')
        succeeded_files.flush()
        #print "*****Final Processing of UVFITS DONE*****"
    except Exception,r:
        process_target_log.write(hostname+"\n"+"Error in final processing of "+str(precal_uvfits)+'\n' + str(r) + '\n' + 55*"*" + '\n')
        process_target_log.flush()
        failed_files.write(CURRENT_DIR + '/' + " : Final Processing error\n")
        failed_files.flush()

    new_files = os.listdir('./')	
    added_files = [z for z in new_files if z not in old_files]
    for added_file in added_files:
        if "obit_lowfrfi.py" not in added_file:
            if ".FITS" in added_file:	
                os.system('mv ' + added_file + ' ' + added_file.replace('.FITS','')+'_'+ lta+ '.FITS')
            if ".UVFITS" in added_file:
                os.system('mv ' + added_file + ' ' + added_file.replace('.UVFITS','')+'_'+ lta + '.UVFITS')

    new_datfil_log = os.listdir('../datfil/')
    added_datfil_log = [z for z in new_datfil_log if z not in old_datfil_log]
    for spam_log in added_datfil_log:
        if 'spam' in spam_log and '.log' in spam_log:
            os.system('mv ../datfil/' + str(spam_log) + ' ./')

    os.chdir('../')

#Location of input data files (changes depending on stage of processing)
#SOURCE_DIR = '/GARUDATA/FITS/SPLIT_LINK_CYCLE_22/'
SOURCE_DIR = src_dir

DATA_FILES = os.listdir(SOURCE_DIR)

THREAD_FILES = []
for FILE in DATA_FILES:
    THREAD_FILES.append(SOURCE_DIR+FILE) 
c_path = os.getcwd()
main()
