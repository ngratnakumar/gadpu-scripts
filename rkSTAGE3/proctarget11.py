import os
import spam
import socket
import sys
import glob
import datetime
import gadpudbapi.project_model_v1 as project_model
import gadpudbapi.tableSch as tableSchema
import random
import time


def __main__():
    spam.set_aips_userid(11)
    hostname = socket.gethostname()
    # Setting the Process Start Date Time
    start_time = str(datetime.datetime.now())
    # Taking system's in/out to backup variable
    original_stdout = sys.stdout
    original_stderr = sys.stderr
    thread_dir = os.getcwd()
    # Changing directory to fits/
    os.chdir("fits/")
    process_target_log = open('process_target.log', 'a+')
    process_target_log.write('\n\n\n******PROCESS TARGET STARTED******\n\n\n')
    process_target_log.write("--->  Start Time " + start_time)
    # Logging Compute Node Hostname
    process_target_log.write("\n Node name" + hostname + "\n")
    # Logging all Standard In/Output
    sys.stdout = process_target_log
    sys.stderr = process_target_log
    datfil_dir = thread_dir + "/datfil/"
    fits_dir = thread_dir + "/fits/"

    db_model = project_model.ProjectModel()
    # Get random imaging_id & project_id
    column_keys = [tableSchema.imaginginputId, tableSchema.projectobsnoId, "calibrated_fits_file"]
    where_con = {
        "status": "unprocessed"
    }
    unprocessed_id_list = db_model.select_from_table("imaginginput", column_keys, where_con, None)
    to_be_processed = random.choice(unprocessed_id_list)
    calibrated_fits_file = to_be_processed["calibrated_fits_file"]
    imaging_id = to_be_processed[tableSchema.imaginginputId]
    project_id = to_be_processed[tableSchema.projectobsnoId]

    # Using the above project_id, fetch base_path
    column_keys = ["base_path"]
    where_con = {
        "project_id": project_id
    }
    base_path = db_model.select_from_table("projectobsno", column_keys, where_con, 0)
    base_path = base_path[0]

    # Update status for imaginginput for selected imaging_id
    current_time_in_sec = time.time()
    current_date_timestamp = datetime.datetime.fromtimestamp(current_time_in_sec).strftime('%Y-%m-%d %H:%M:%S')
    update_data = {
        "set": {
            "status": "processing",
            "start_time": current_date_timestamp
        },

        "where": {
            "imaging_id": imaging_id,
            "project_id": project_id
        }
    }
    db_model.update_table(update_data, "imaginginput")

    # Insert new thread to computethread
    column_keys = [tableSchema.computenodeId, "threads_count"]
    where_condition = {
        "node_name": hostname
    }
    node_details = db_model.select_from_table("computenode", column_keys, where_condition, None)

    node_id = node_details[0][tableSchema.computenodeId]
    threads_count = node_details[0]["threads_count"]
    computethread_data = {
        'pipeline_id': 1,
        'node_id': node_id,
        'thread_dir': thread_dir,
        'status': 'processing',
        'file_name': calibrated_fits_file,
        'comments': "{'imaging_id': "+str(imaging_id)+", 'project_id': "+str(project_id)+"}"
    }
    thread_id = db_model.insert_into_table("computethread", computethread_data, tableSchema.computethreadId)
    # Update computenode with the above generated node_id & increment threads_count
    node_update_data = {
        "set": {
            "threads_count": threads_count+1,
            "status": "processing"
        },
        "where": {
            "node_id": node_id
        }
    }
    db_model.update_table(node_update_data, "computenode")
    uvfits_full_path = base_path+"/PRECALIBRATED/"+calibrated_fits_file
    print "Copying " + uvfits_full_path + " to " + fits_dir
    copying_fits = os.system("cp " + uvfits_full_path + " " + fits_dir)
    uvfits_file = calibrated_fits_file
    # Starting spam.process_target(SPLIT_FITS_FILE)
    try:
        spam.process_target(uvfits_file, allow_selfcal_skip=True)
        # If this process_target is success call
        # GADPU API setSuccessStatus for the current fits_id
        current_time_in_sec = time.time()
        current_date_timestamp = datetime.datetime.fromtimestamp(current_time_in_sec).strftime('%Y-%m-%d %H:%M:%S')
        success_update_data = {
            "set": {
                "status": "success",
                "end_time": current_date_timestamp
            },
            "where": {
                "imaging_id": imaging_id
            }
        }
        db_model.update_table(success_update_data, "imaginginput")
        db_model.update_table(
            {
                "set": {
                    "status": "success"
                },
                "where":
                    {
                        "thread_id": thread_id
                    }
            }, "computethread")

        print("spam.process_tagret " + uvfits_file)
    except Exception, e:
        process_target_log.write("Error: " + str(e))
        # If this process_target is a failure call
        # GADPU API setFailedStatus for the current fits_id
        current_time_in_sec = time.time()
        current_date_timestamp = datetime.datetime.fromtimestamp(current_time_in_sec).strftime('%Y-%m-%d %H:%M:%S')
        success_update_data = {
            "set": {
                "status": "failed",
                "end_time": current_date_timestamp
            },
            "where": {
                "imaging_id": imaging_id
            }
        }
        db_model.update_table(success_update_data, "imaginginput")
        db_model.update_table(
            {
                "set": {
                    "status": "failed"
                },
                "where":
                    {
                        "thread_id": thread_id
                    }
            }, "computethread")
        print("Error: spam.process_tagret Failed " + uvfits_file)
        # Even the process is Success/Failed we remove
        # the Initially copied SPLIT_FITS_file, to save
        # disk space
    os.system('rm ' + uvfits_file)
    col_key = ["threads_count"]
    node_where = {
        "node_id": node_id
    }
    node_thread_count = db_model.select_from_table("computenode", col_key, node_where, 0)
    threads_count = node_thread_count[0]
    node_update_data = {
        "set": {
            "threads_count": threads_count-1,
            "status": "processing"
        },
        "where": {
            "node_id": node_id
        }
    }
    db_model.update_table(node_update_data, "computenode")

    # recording the process end time to Log
    end_time = str(datetime.datetime.now())
    image_base_dir = base_path+"/FITS_IMAGE/"
    # Creating a new dir at BASE_DIR - FITS_IMAGES
    print "Make dir at " + image_base_dir
    os.system("mkdir -p " + image_base_dir)
    # STDIN/OUT controls are reverted.
    print "Removing the " + thread_dir + " from " + hostname
    process_target_log.write("End Time " + end_time)
    # Flushing the all processed out log to the log_file
    process_target_log.flush()
    # reverting stdin/out controls
    sys.stdout = original_stdout
    sys.stderr = original_stderr
    # Getting the list of datfil/spam_logs for summarize the process
    spam_log = glob.glob(datfil_dir + "spam*.log")
    # if spam_log is non-empty list, procced
    print spam_log
    if spam_log:
        # for every spam*.log file in the datfile file
        for each_spam_log in spam_log:
            original_stdout = sys.stdout
            original_stderr = sys.stderr
            failed = os.popen('grep "processing of field" ' + each_spam_log + ' | grep "failed" | wc -l').read()
            if int(failed.strip()) > 0:
                success_update_data = {
                    "set": {
                        "status": "failed",
                        "end_time": current_date_timestamp
                    },
                    "where": {
                        "imaging_id": imaging_id
                    }
                }
                db_model.update_table(success_update_data, "imaginginput")
            print each_spam_log
            # getting summary of the log file
            summ_file = each_spam_log.replace(".log", ".summary")
            print summ_file
            summary_filename = open(summ_file, 'a+')
            # making the spam*.summary file and write the
            # summarize_spam_log output
            summary_filename.write('\n\n******SUMMARY LOG******\n\n')
            sys.stdout = summary_filename
            sys.stderr = summary_filename
            spam.summarize_spam_log(each_spam_log)
            sys.stdout = original_stdout
            sys.stderr = original_stderr
            summary_filename.flush()
    # Once the summary file is created inside the fits/
    # Moving all the files from datfil/ to fits/
    # Moving all the processed files from fits/ to FITS_IMAGE@BASE_DIR
    print "moving back the processed files from " + fits_dir + " to " + image_base_dir
    # The below print statement is only for recording purpose,
    # actual removing the THREAD directory is done after the
    # Move all the fits/ to FITS_IMAGE@BASE_DIR
    print "Moving datfil/ to fits/"
    movedata = os.system('mv ' + datfil_dir + '* ' + fits_dir)
    movefits = os.system("mv " + fits_dir + "* " + image_base_dir)
    # Changing the directory to /home/gadpu, inorder to delete the
    # current THREAD dir
    os.chdir('../../')
    print "Changed to " + os.getcwd()
    # Removing the current THREAD directory

    # delete_thread_process = subprocess.Popen(['rm', '-rf', thread_dir])
    # delete_thread_process.communicate()

    removethread = os.system('rm -rf ' + thread_dir)
    # exiting the SPAM process and cleaning the cache memory
    print "Move failed"
    spam.exit()


if __name__ == '__main__':
    __main__()
