import os
import glob
from gadpudbapi.requests_api import getRequest, postRequest

path = "/GARUDATA/IMAGING24/CYCLE24/"
pc = "/PRECALIBRATED/*RRLL*.UVFITS"
pt = "/FITS_IMAGES/*PBCOR*.FITS"
proposal_dir_list = glob.glob(path + "*")

success_list = []
failed_list = []
fits_ids = []
for pdl in proposal_dir_list:
    pc_dir_list = glob.glob(pdl + pc)
    if pc_dir_list:
        for pcdl in pc_dir_list:
            pcdli = pcdl.split('/')[6].split('_')[0]
            pt_dir_list = glob.glob(pdl + pt)
            # print "Precal " +pcdli
            for ptdl in pt_dir_list:
                ptdli = ptdl.split('/')[6].split('.')[0]
                # print "ProcTar " +ptdli
                if pcdli == ptdli:
                    success_list.append(pcdli)
                    base_path = pcdl.split('PRECALIBRATED')[0]
                    uvfits_name = pcdl.split('PRECALIBRATED')[1]
                    # print "PT : " + str(ptdl.split('FITS_IMAGES'))
                    fits_id = postRequest("getfitsidbyfile", [base_path[:-1], uvfits_name[1:]])[0]
                    if fits_id != 'None':
                        fits_ids.append(fits_id)
                else:
                    failed_list.append(pcdli)

print len(success_list)
print len(failed_list)
print len(fits_ids)
print fits_ids
