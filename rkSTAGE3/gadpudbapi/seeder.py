import glob
import project_model_old

BASE_PATH = "/GARUDATA/IMAGING24/CYCLE24/"
CYCLE_ID = 24

RUN_GADPU_SEEDING = True


def gadpu_seeding():

    model = project_model_old.ProjectModel()

    seed_lta_uvfits = True
    seed_pre_calibration = True
    seed_process_targets = True
    seed_gadpu_images = False

    precalibration_status_file = "precalibration"

    pre_calibrated_uvfits_dir = "/PRECALIBRATED/"
    process_target_fits_dir = "/FITS_IMAGES/"
    ALL_PROJECTS = glob.glob(BASE_PATH + "*")

    lta_id = 0
    for each_project in ALL_PROJECTS:
        proposal_code = each_project.split('/')[4]
        proposal_base_path = each_project
        proposal_obslog_file = glob.glob(each_project + "/*.obslog")[0].split('/')[5]
        # inserting new record for the current PROPOSAL_CODES
        project_id = model.insert_project(proposal_code, proposal_base_path, CYCLE_ID, proposal_obslog_file)
        proposal_lta_files = glob.glob(each_project + "/*.lta*")
        proposal_lta_files.sort()
        lta_list = []
        uvfits_list = []
        if seed_lta_uvfits:
                # parsing the list of all PROPOSAL_CODES
            for each_file in proposal_lta_files:
                # getting only lta files list not UVFITS
                if "UVFITS" not in each_file:
                    to_be_processed = True
                    lta_uvfits_status = "success"
                    lta_comb_status = "success"
                    lta_status = "good"
                    # Inserting new record to lta_uvfits table with lta_file
                    # project_id, lta_name, lta_uvfits_status, to_be_processed, lta_status
                    lta_id = model.insert_lta_uvfits(project_id, each_file.split('/')[5], lta_uvfits_status, to_be_processed, lta_status)
                # if the proposal_code directory has UVFITS files
                if "UVFITS" in each_file and lta_id and seed_pre_calibration:
                    uvfits_list = glob.glob(each_project + "/*.UVFITS*")
                    if uvfits_list:
                        uvfits_file = each_file.split('/')[5]
                        # if uvfits_file is not empty, insert uvfits_file name to pre_calibration table
                        if uvfits_file:
                            is_pre_calibrated = False
                            pre_calibration_status = ""
                            status_file = glob.glob(each_project + "/" + precalibration_status_file + "*")[0]
                            if status_file:
                                pre_calibration_status = status_file.split('/')[5].split('.')[1]
                                if "success" in pre_calibration_status or "failed" in pre_calibration_status:
                                    uvfits_id = model.insert_pre_calibration(project_id, lta_id, uvfits_file, pre_calibrated_uvfits_dir, pre_calibration_status)
                            if uvfits_id and seed_process_targets:
                                # if uvfits_id is not empty, insert the split fits file name to preocess_target table
                                fits_files = glob.glob(each_project + pre_calibrated_uvfits_dir + "*RRLL*P*UVFITS")
                                if fits_files:
                                    for each_fits in fits_files:
                                        # /GARUDATA/IMAGING24/CYCLE24/24_053_23APR13/PRECALIBRATED/SGRA_GMRT325_20130422_RRLL_USB_3C286P.UVFITS
                                        process_tagret_status = ""
                                        fits_name = each_fits.split('/')[6]
                                        fits_id = model.insert_process_target(project_id, lta_id, uvfits_id, fits_name, process_target_fits_dir, process_tagret_status)
                                        print fits_id


if __name__ == '__main__':
    if RUN_GADPU_SEEDING:
        gadpu_seeding()
    else:
        print "=========> RUN_GADPU_SEEDING is set to ", RUN_GADPU_SEEDING
