import web
import project_model_old

urls = (
    '/', 'index',
    '/getfitsid', 'getfitsid',
    '/getfitsdetails', 'getfitsdetails',
    '/setfitsstatus', 'setfitsstatus',
    '/setProcessingStatus', 'setProcessingStatus',
    '/setSuccessStatus', 'setSuccessStatus',
    '/setfailedStatus', 'setfailedStatus',
    '/setStartThread', 'setStartThread',
    '/postNodeCount', 'postNodeCount',
    '/getprocessedfitsid', 'getprocessedfitsid',
    '/getfitsidbyfile', 'getfitsidbyfile',
    '/test', 'test'
)


class index:
    def GET(self):
        return "Hello, world!"


class getfitsidbyfile:
    def POST(self):
        data = web.input()
        model = project_model_old.ProjectModel()
        fits_id = model.get_fitsid_by_file(data['data'])
        return fits_id
        # return model.get_fitsid_by_file(data['data'])


class getfitsid:
    def GET(self):
        model = project_model_old.ProjectModel()
        fits_id = model.get_unprocessed_fits_id()
        return fits_id


class setfitsstatus:
    def POST(self):
        data = web.input()
        model = project_model_old.ProjectModel()
        return data


class setProcessingStatus:
    def POST(self):
        data = web.input()
        model = project_model_old.ProjectModel()
        return model.update_process_target(data['data'], "processing")


class setSuccessStatus:
    def POST(self):
        data = web.input()
        model = project_model_old.ProjectModel()
        return model.update_process_target(data['data'], "success")


class setfailedStatus:
    def POST(self):
        data = web.input()
        model = project_model_old.ProjectModel()
        return model.update_process_target(data['data'], "failed")


class getfitsdetails:
    def POST(self):
        data = web.input()
        model = project_model_old.ProjectModel()
        return model.get_fits_details(data['data'])


class getprocessedfitsid:
    def POST(self):
        data = web.input()
        model = project_model_old.ProjectModel()
        return model.get_processed_fits_id(data['data'])


'''
    if process_status is "processing":
        postRequest('/setStartThread')
    if process_status is "success":
        postRequest('/setThreadSuccess')
    if process_status is "success":
        postRequest('/setThreadFailed')

'''


class setStartThread:
    def POST(self):
        data = web.input()
        model = project_model_old.ProjectModel()
        # return model.insert_compute_threads(node_name, thread_dir, status, stage_name, file_name)(data['data'], "processing")
        return data['data']


class postNodeCount:
    def POST(self):
        data = web.input()
        model = project_model_old.ProjectModel()
        thread_count = model.get_compute_node_details(data['data'])
        return thread_count


if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
