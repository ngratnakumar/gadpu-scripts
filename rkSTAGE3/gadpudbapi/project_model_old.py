#!/usr/bin/python
import psycopg2
import random
from config import config

"""
********************************* projects table************************************
"""


class ProjectModel():
    """ProjectModel is the Base model that does CRUD OPerations on Gadpu Database"""

    def run_query(self, sql, values, result_type):
        """ run_query will run all the CRUS operation queries 
        result_type takes 
            1 fetchone - fetchone()[0] insert or update or select only one column and row
            2 fetchall - for select query
            2 fetchrow - for select single row 

        """
        sql = """INSERT INTO projects(proposal_code, proposal_base_path, cycle_id, observation_log)
                VALUES(%s,%s,%s,%s) RETURNING project_id;"""
        conn = None
        returning = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the INSERT statement
            cur.execute(sql, values)
            # get the generated id back
            if result_type == "fetchone":
                returning = cur.fetchone()[0]
            elif result_type == "fetchrow":
                returning = cur.fetchone()
            else:
                returning = cur.fetchall()

            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        print returning
        return returning

    def update_query(self):
        pass

    def select_query(self):
        pass

    def insert_project(self, proposal_code, proposal_base_path, cycle_id, observation_log):
        """Insert a new proposal into project table"""
        sql = """INSERT INTO projects(proposal_code, proposal_base_path, cycle_id, observation_log)
                VALUES(%s,%s,%s,%s) RETURNING project_id;"""
        conn = None
        project_id = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the INSERT statement
            cur.execute(sql, (proposal_code, proposal_base_path, cycle_id, observation_log))
            # get the generated id back
            project_id = cur.fetchone()[0]
            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        return project_id

    # def insert_project(self, proposal_code, proposal_base_path, cycle_id, observation_log):
    #     """Insert a new proposal into project table"""
    #     sql = """INSERT INTO projects(proposal_code, proposal_base_path, cycle_id, observation_log)
    #             VALUES(%s,%s,%s,%s) RETURNING project_id;"""
    #     data = (proposal_code, proposal_base_path, cycle_id, observation_log)
    #     return self.run_query(sql, data)

    """
    ********************************* lta_uvfits table************************************
    """

    def insert_lta_uvfits(self, project_id, lta_name, lta_uvfits_status, to_be_processed, lta_status):
        """Insert a new proposal into lta_uvfits table"""
        sql = """INSERT INTO lta_uvfits(project_id,lta_name, lta_uvfits_status, to_be_processed, lta_status)
                VALUES(%s,%s,%s,%s,%s) RETURNING lta_id;"""
        conn = None
        lta_id = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the INSERT statement
            cur.execute(sql, (project_id, lta_name, lta_uvfits_status, to_be_processed, lta_status))
            # get the generated id back
            lta_id = cur.fetchone()[0]
            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        return lta_id

    """
    ********************************* pre_calibration table************************************
    """

    def insert_pre_calibration(self, project_id, lta_id, uvfits_name, pre_calibrated_uvfits_dir, pre_calibration_status):
        """Insert a new proposal into pre_calibration table"""
        sql = """INSERT INTO pre_calibration(project_id,lta_id,uvfits_name,pre_calibrated_uvfits_dir,pre_calibration_status)
                VALUES(%s,%s,%s,%s,%s) RETURNING uvfits_id;"""
        conn = None
        uvfits_id = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the INSERT statement
            cur.execute(sql, (project_id, lta_id, uvfits_name, pre_calibrated_uvfits_dir, pre_calibration_status))
            # get the generated id back
            uvfits_id = cur.fetchone()[0]
            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        return uvfits_id

    def select_pre_calibration(self,):
        sql = """SELECT * FROM pre_calibration WHERE pre_calibration_status is NULL AND is_pre_calibrated is True;"""
        conn = None
        data = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the INSERT statement
            cur.execute(sql)
            # get all rows - fetchall()
            data = cur.fetchall()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        return data

    def update_pre_calibration(self,):
        pass

    """
    ********************************* process_target table************************************
    """

    def insert_process_target(self, project_id, lta_id, uvfits_id, fits_name, process_target_fits_dir, process_target_status):
        """Insert a new proposal into process_target table"""
        sql = """INSERT INTO process_target(project_id,lta_id,uvfits_id,fits_name,process_target_fits_dir, process_target_status)
                VALUES(%s,%s,%s,%s,%s,%s) RETURNING fits_id;"""
        conn = None
        fits_id = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the INSERT statement
            cur.execute(sql, (project_id, lta_id, uvfits_id, fits_name, process_target_fits_dir, process_target_status))
            # get the generated id back
            fits_id = cur.fetchone()[0]
            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        return fits_id

    def update_process_target(self, fits_id, process_target_status):
        """Update the process_target_status in process_target table"""
        sql = "UPDATE process_target SET process_target_status = '" + process_target_status + "',run_count = 2 WHERE fits_id = " + str(fits_id) + ";"
        conn = None
        updated_rows = ""
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the UPDATE statement
            cur.execute(sql)
            # get the updated row counts
            updated_rows = cur.rowcount
            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        return updated_rows

    """
    ********************************* get details************************************
    """

    def get_processed_fits_id(self,):
        sql = "SELECT fits_id FROM process_target WHERE (process_target_status = '' OR  process_target_status like 'success')"
        conn = None
        fits_id = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the INSERT statement
            cur.execute(sql)
            # get the generated id back
            fits_result = cur.fetchall()
            random.shuffle(fits_result)
            fits_id = fits_result[0][0]
            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        return fits_id

    def get_unprocessed_fits_id(self,):
        sql = "SELECT fits_id FROM process_target WHERE (process_target_status = '' OR  process_target_status = NULL)"
        conn = None
        fits_id = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the INSERT statement
            cur.execute(sql)
            # get the generated id back
            fits_result = cur.fetchall()
            random.shuffle(fits_result)
            fits_id = fits_result[0][0]
            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        return fits_id

    def get_fits_details(self, fits_id):
        sql = "SELECT fits_id,proposal_base_path,pre_calibrated_uvfits_dir,fits_name,process_target_fits_dir FROM process_target inner join projects on process_target.project_id = projects.project_id INNER JOIN pre_calibration on pre_calibration.project_id = process_target.project_id WHERE (process_target_status = '' OR  process_target_status = NULL) AND fits_id = " + str(fits_id)
        conn = None
        fits_details = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the INSERT statement
            cur.execute(sql)
            # get the generated id back
            fits_details = cur.fetchone()
            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        return fits_details

    def get_fitsid_by_file(self, data):
        """Update the process_target_status in process_target table"""
        n_data = data[1:-1]
        data = n_data.split(',')
        print data
        base_path = data[0]
        fits_name = data[1]
        print base_path, fits_name
        sql = "SELECT fits_id from process_target pt inner join projects p on p.project_id = pt.project_id where p.proposal_base_path like {} AND pt.fits_name like {};".format(base_path, fits_name.replace("/", ""))
        # print sql
        conn = None
        fits_id = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the UPDATE statement
            cur.execute(sql)
            # get the updated row counts
            fits_id = cur.fetchall()[0]
            print fits_id
            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        return fits_id

    def get_compute_node_details(self, node_name):
        sql = "SELECT * FROM compute_nodes WHERE node_name like '" + node_name + "';"
        print node_name
        conn = None
        node_details = None
        try:
            # read database configuration
            params = config()
            # connect to the PostgreSQL database
            conn = psycopg2.connect(**params)
            # create a new cursor
            cur = conn.cursor()
            # execute the INSERT statement
            cur.execute(sql)
            # get the generated id back
            node_details = cur.fetchone()
            # commit the changes to the database
            conn.commit()
            # close communication with the database
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        return node_details
    """
    ********************************* compute_threads table************************************
    """

    def insert_compute_threads(self, node_name, thread_dir, stage_name, fits_id):
        """Insert a new thread into compute_threads table"""
        model = ProjectModel()
        node = model.get_compute_node_details(node_name)
        reboot = node[4]
        node_status = node[3]
        status = "processing"
        if "faulty" in node_status or reboot:
            print "Skipping THREAD creation"
            return (0, 0)
        else:
            if stage_name == "pre_calibration":
                pass
            elif stage_name == "process_target":
                # fits_id = get_unprocessed_fits_id()
                fits = model.get_fits_details(fits_id)
                # update_process_target(fits_id, "processing")
                print "updte process_target " + fits_id + " to processing"
                file_name = fits[3]
            node_id = node[0]
            sql = """INSERT INTO compute_threads(node_id, thread_dir, status, stage_name, file_name)
                    VALUES(%s,%s,%s,%s,%s) RETURNING thread_id;"""
            conn = None
            thread_id = None
            try:
                # read database configuration
                params = config()
                # connect to the PostgreSQL database
                conn = psycopg2.connect(**params)
                # create a new cursor
                cur = conn.cursor()
                # execute the INSERT statement
                cur.execute(sql, (node_id, thread_dir, status, stage_name, file_name))
                # get the generated id back
                thread_id = cur.fetchone()[0]
                # commit the changes to the database
                conn.commit()
                # close communication with the database
                cur.close()
                model.update_compute_nodes(node_name, status)
            except (Exception, psycopg2.DatabaseError) as error:
                print(error)
            finally:
                if conn is not None:
                    conn.close()
            return thread_id, fits_id

    """
    ********************************* compute_nodes table************************************
    """

    def update_compute_nodes(self, node_name, set_status):
        """Update the process_target_status in process_target table"""
        model = ProjectModel()
        node = model.get_compute_node_details(node_name)
        node_id = node[0]
        threads = int(node[2])
        threads += 1
        if node[4]:
            print "This Node is getting Rebooted"
            return 0
        else:
            if threads == 6:
                reboot_flag = True
                status = "rebooting"
            else:
                reboot_flag = False
                if threads == 0:
                    status = "ready"
                else:
                    status = "processing"

            if "NA" not in set_status:
                status = set_status

            sql = "UPDATE compute_nodes SET status = '" + status + "', threads_count = '" + str(threads) + "', reboot_flag = " + str(reboot_flag) + " WHERE node_id = " + str(node_id) + ";"
            conn = None
            updated_rows = 0
            try:
                # read database configuration
                params = config()
                # connect to the PostgreSQL database
                conn = psycopg2.connect(**params)
                # create a new cursor
                cur = conn.cursor()
                # execute the UPDATE statement
                cur.execute(sql)
                # get the updated row counts
                updated_rows = cur.rowcount
                # commit the changes to the database
                conn.commit()
                # close communication with the database
                cur.close()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error)
            finally:
                if conn is not None:
                    conn.close()

            return updated_rows

        return node_name


# def main():
#     #     print "ProjectModel loaded"
#     # print get_unprocessed_fits_id()
#     # print update_compute_nodes("garuda-066")
#     # print get_compute_node_details("garuda-066")
#     # val = insert_compute_threads("garuda-095", "/home/gadpu/THREAD13", "NA", "process_target", "NA")
#     # print val[0]
#     # print val[1]
#     # print get_compute_node_details(66)
#     # print get_fits_details(321)
#     # print update_process_target(321, "processing")
#     model = ProjectModel()
#     project_id = model.insert_project("24_055", "/GARUDATA/IMAGING24/CYCLE24/24_055_06SEP13/", "24", "12432.obslog")
#     print "Inserted project with project_id = ", project_id
#     # insert_pre_calibration("1", "1", "uvfits_name", "pre_calibrated", "pre_calibration", True)


# if __name__ == '__main__':
#     main()
