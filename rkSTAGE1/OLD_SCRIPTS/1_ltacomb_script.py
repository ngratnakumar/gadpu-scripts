import os
import glob
import pandas as pd
import subprocess
from config import *

RUN_LTACOMB=false
RUN_SCRIPTER=true

def extract( file_name ):
    with open(file_name) as f:
        for i,line in enumerate(f,1):
            if "SCN" in line:
                return i

def runCommand(cmd, outfile):
    try:
        if cmd is not None : cmdStr=str(cmd)
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=False)
        (out, err) = proc.communicate()
        logging.info(cmdStr+" Success")
#	logging.info("Running command "+cmd[0]+" on file "+cmd[2]+"\t: SUCCESS!")
	if outfile is not None:
		open(outfile,'w').write(out)
    except: 
        logging.error("Error: "+cmdStr+"\n"+str(err)+"\n")
        pass

def check_RF(directory_path):
	RF = []
	temp_list = []
	os.chdir(directory_path)
	lta_list = glob.glob('*.lta*')
        for lta_file in lta_list:
		cmd=['ltahdr','-i',lta_file]
		runCommand(cmd, directory_path+'/lta_file.txt')
		#os.system('ltahdr -i ' + lta_file + ' >lta_file.txt')
                try
			skipped_rows = extract('lta_file.txt') - 1
			header = pd.read_csv('lta_file.txt',skiprows=skipped_rows,delimiter=r"\s+")
			print list(set(header["RF(MHz)"]))
			RF += list(set(header["RF(MHz)"]))
			if(len(list(set(RF)))==1):
				if(len(lta_list)!=1):
					print 'Equal'
					lta_list.sort()
					temp_list = ','.join(map(str,lta_list))
					print temp_list
	                	        #os.system('ltacomb -i '+ temp_list)
        	                	print "Running LTACOMB "+temp_list
		                        cmd=['ltacomb', '-i', temp_list]
		                        runCommand(cmd, None)
	        	                #try:
		        	        #        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=False)
	        	        	#        (out, err) = proc.communicate()
					#	log_file.write("Success: ltacomb "+temp_list+"\n")
					#except:
					#	log_file.write("Error: ltacomb "+temp_list+"\n"+out+"\n")
					os.system('rm -f ' + ' '.join(lta_list))
					os.system('mv ltacomb_out.lta ' + str(lta_list[0]))
					#os.system('touch "combined"')
					print directory_path
                except TypeError:
			logging.error("Extract File Error: "+lta_file+"\n")
	else:
		print 'Unequal'

#directory_path = '/GARUDATA/LTA/CYCLE22/'
directory_path = filtered_lta_data
directory_list = os.listdir(directory_path)
if $RUN_LTACOMB; then
	for directory in directory_list:
		check_RF(directory_path+directory)
fi

if $RUN_SCRIPTER; then
	# below code is from 2_scripter.py 
	#dest = working_data  
	dest = working_data 
	#source = processed_lta_data # instead of processed_lta_data it should be filtered_lta_data?
	source = filtered_lta_data
	THREAD_FILES = os.listdir(source)

	for directory in THREAD_FILES:
	        os.chdir(source+directory)
	        files = glob.glob('*.lta*')
	        for file in files:
	                dir_name = file.replace('.','_')
	                if 'comb' in dir_name:	
	                        dir_name.replace('comb_','')
	                os.system('mkdir ' + dest + dir_name)
	                os.system('mv ' + file + ' ' + dest + dir_name)

