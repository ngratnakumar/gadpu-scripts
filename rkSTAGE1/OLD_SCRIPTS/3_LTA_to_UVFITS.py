import re
import os
import glob
import spam
from config import *

# data_dir = '/GARUDATA/LTA/datum/'

data_dir = working_data   #directory containing cycle data
os.chdir(data_dir)
all_observations = os.listdir(data_dir)
all_observations.sort()
for DIR_NAME in all_observations:
	os.chdir(data_dir+DIR_NAME+'/')
	lta_files = glob.glob('*.lta*')
	uvfits_files = glob.glob('*.UVFITS*')
	if len(uvfits_files)==0:
#		flag_files = glob.glob('*.FLAGS*')
		for i in range(len(lta_files)):
			lta_file_name = lta_files[i]
			uvfits_file_name = lta_files[i]+'.UVFITS'
			try:
				spam.convert_lta_to_uvfits( lta_file_name, uvfits_file_name )
				#os.system('rm -f ' + lta_file_name)
			except Exception,r:
				os.chdir(data_dir+DIR_NAME+'/')
				failed_file = open('failed_file_log.txt','a+')
				failed_file.write(str(r)+'\n')
				failed_file.close()
