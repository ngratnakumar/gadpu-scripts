import filter_lta
import os, re
import glob
import sys
from config import *

#List of all directories containing valid observations 
VALID_FILES = filter_lta.VALID_OBS()
#List of all directories for current threads to process
#THREAD_FILES = VALID_FILES

#dest = '/GARUDATA/LTA/datum/'
#source = '/GARUDATA/LTA/FINAL_CYCLE22/'


dest = working_data
source = processed_lta_data
THREAD_FILES = os.listdir(source)
#print THREAD_FILES

#directories = os.listdir(source)

for directory in THREAD_FILES:
	os.chdir(source+directory)
	files = glob.glob('*.lta*')
	for file in files:
		dir_name = file.replace('.','_')
		if 'comb' in dir_name:
			dir_name.replace('comb_','')
		os.system('mkdir ' + dest + dir_name)
		os.system('mv ' + file + ' ' + dest + dir_name)
