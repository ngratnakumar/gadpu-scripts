import requests
hostname = "192.168.110.121"
port = "9099"


def getRequest(type):
    result = requests.get("http://" + hostname + ":" + port + "/" + type)
    return result


def postRequest(type, data):
    data = "data={lst}".format(lst=data)
    result = requests.post("http://" + hostname + ":" + port + "/" + type, data)
    proc_data = result.content.replace("[", "").replace("]", "").split(',')
    data = []
    for val in proc_data:
        val = val.replace("'", " ").replace("(", "").replace(")", "").strip()
        data.append(val)
    return data
