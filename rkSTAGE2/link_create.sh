#Creating links with only Primary UVIFTS files(*RRLL*P*)
j=1
for i in `ls /FITS/data/SPLIT_CYCLE_23/*/*RRLL*P*`
do
    mkdir $j
    cd $j
    ln -s $i `echo $i | cut -d'/' -f6`
    j=$((j+1))
    cd ../
done

