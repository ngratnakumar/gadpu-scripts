import spam
import time
import random
import os, re
import glob
import trace
import sys
import time,datetime
import socket
from config import *

hostname=socket.gethostname()
spam.set_aips_userid(19)

failed_files = open('datfil/failed_files.log','a+')
succeeded_files = open('datfil/succeeded_files.log','a+')
succeeded_files.write('Successful final processing\n\n')
succeeded_files.flush()

failed_files.write('Failed files\n\n')
failed_files.flush()
directory_log_pre = open('directory_log_pre.txt','a+')
directory_log_post = open('directory_log_post.txt','a+')
os.system('pwd')

#Location of input data files (changes depending on stage of processing)
#SOURCE_DIR = '/GARUDATA/LTA/datum/'
SOURCE_DIR = working_data
#DEST_DIR = '/GARUDATA/FITS/SPLIT_CYCLE_22/'
DEST_DIR = split_data
DATA_FILES = os.listdir(SOURCE_DIR)

THREAD_FILES = []
for FILE in DATA_FILES:
    THREAD_FILES.append(SOURCE_DIR+FILE) 
random.shuffle(THREAD_FILES)
c_path = os.getcwd()

def precalibrator(CURRENT_DIR):
    os.chdir('fits/')
    original_stdout = sys.stdout
    original_stderr = sys.stderr
    l2u_precal_log = open('l2u_precal.log','a+')
    l2u_precal_log.write('\n\n\n******PRECALIBRATION STARTED******\n\n\n')
    sys.stdout = l2u_precal_log
    sys.stderr = l2u_precal_log
    uvfits_files = glob.glob('*.UVFITS')
    #print uvfits_files
    final_list = []
    precalibrator_log = open('precalibrator_log.txt', 'a+')
    if uvfits_files != []:
        try:
            #To get a list of newly added files after precalibration
            spam.pre_calibrate_targets(uvfits_files[0])
            precalibrator_log.write(hostname + '\n' + "Successful precalibration of " + str(uvfits_files[0])+ '\n' + 55*'*' + '\n')
            precalibrator_log.flush()
            os.remove(str(uvfits_files[0]))
        except Exception,r:
            precalibrator_log.write(hostname+"\n"+"Error in precalibration of " + str(uvfits_files[0]) + '\n' +str(r)+'\n'+55*'*'+'\n')
            precalibrator_log.flush()
            failed_files.write(CURRENT_DIR + '/' +str(uvfits_files[0]).replace('.UVFITS','')+" : Precalibration error\n")
            failed_files.flush()
    else:
        failed_files.write(CURRENT_DIR + '/' +" : UVFITS FILE not found, LTA_to_UVFITS no output\n")
        failed_files.flush()
        precalibrator_log.write("Error in precalibration due to no UVFITS file " + '\n' + 55*'*' + '\n')
    sys.stdout = original_stdout
    sys.stderr = original_stderr
    os.chdir('../')

#Wrapper function to create expected environment before calling precalibrate
#i.e. Write valid log, replace file names, move final split files, etc.
def PREPROCESS(CURRENT_DIR):
    start = time.time()
    os.system('touch ' + CURRENT_DIR +'/valid.log')
    os.chdir(CURRENT_DIR+'/')

    uv_fits_list = glob.glob('*.UVFITS')
    lta_list = uv_fits_list[0].replace('.UVFITS','') #since there's only one single uvfits file
    os.chdir(c_path +'/')

    os.system('cp ' + CURRENT_DIR + '/' + uv_fits_list[0] + ' fits/')

    before_precal = os.listdir('fits/')
    precalibrator(CURRENT_DIR)   #can be optimized, dont need to write them in the file uvfits_newuvfits
    after_precal = os.listdir('fits/')
    added_precal = [z for z in after_precal if z not in before_precal]

    directory_log_post.write(CURRENT_DIR + '\n')
    directory_log_post.flush()

    os.chdir('fits/')
    for precal_file in added_precal:
        os.system('mv ' + precal_file + ' ' + precal_file.replace('.UVFITS','') + '_' + lta_list[0].replace('.','_')+ '.UVFITS')
    os.chdir('../')
    end = time.time()
    os.chdir('fits/')
    timefile = open('time_process.txt','a+')
    timefile.write(str(datetime.timedelta(seconds=end-start)))
    timefile.flush()
    os.system('mkdir -p ' + DEST_DIR + lta_list.replace('.','_'))
    os.system('mv * ' + DEST_DIR + lta_list.replace('.','_'))

def main():	 
    #Copy all LTA files in valid obs dir to fits directory for processing
    for CURRENT_DIR in THREAD_FILES:
        #If valid.log present in directory, skip the directory else process
        if "valid.log" not in os.listdir(CURRENT_DIR+'/'):
            directory_log_pre.write(CURRENT_DIR + '\n')
            directory_log_pre.flush()
            PREPROCESS(CURRENT_DIR)
            sys.exit()
        else:
            continue

main()
