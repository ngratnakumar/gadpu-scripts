#!/bin/bash

export SPAM_PATH=/export/spam
export SPAM_HOST=GADPU
export PYTHON=/export/spam/Python-2.7/bin/python
export PYTHONPATH=${SPAM_PATH}/python:${PYTHONPATH}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${SPAM_PATH}/lib
export PATH=${SPAM_PATH}/bin:${PATH}

# TODO: add the following stage2_processing_files.list creation
# at the end of STAGE1 - STEP2(LTATOUVFITS)
#ls /GARUDATA/IMAGING24/CYCLE24/*/*.uvfits > /GARUDATA/IMAGING24/stage2_processing_files.list

i=11
while [ $i -lt 19 ]
do
	cat 'precalib'$i.py | sed s/$i/$[$i+1]/ > 'precalib'$[$i+1].py
	#cat 'parsel_wrapper_'$i.sh | sed s/$i/$[$i+1]/g > 'parsel_wrapper_'$[$i+1].sh
	i=$[$i+1]
	chmod +x precalib*.py
done



i=11
while [ $i -lt 17 ]
do
	start_parseltongue.sh THREAD$i/ $i ../precalib$i.py & 
	i=$[$i+1]
	sleep 30
done

