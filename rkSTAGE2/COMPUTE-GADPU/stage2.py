from config import *
import spam

aips_id = int(socket.gethostname().split('-')[1])
spam.set_aips_userid(aips_id)

curr_dir = os.getcwd()
hostname = socket.gethostname()


def remove_uvfits_from_assignedList(uvfits):
    delString = uvfits.strip().split('/')[4]
    '''
    delString.append(uvfits)
    fileList = readFileToList(STAGE2_PROCESSING_FILE_LIST + "new")
    print(len(fileList))
    # newFileList = open(STAGE2_PROCESSING_FILE_LIST, "w")
    # print "removing " + delString
    # for file in fileList:
    #     if uvfits not in file:
    #         print file + "" + uvfits
    #         newFileList.write(file)
    # newFileList.close()
    with open(STAGE2_PROCESSING_FILE_LIST) as oldfile, open(STAGE2_PROCESSING_FILE_LIST + "new", 'w') as newfile:
        print "OLD UVFITS: " + uvfits + " ||"
        uvfits = uvfits.strip().split('/')[4]
        print "NEW UVFITS: " + uvfits + " ||"
        for line in oldfile:
            #            if not any(delMe in line for delMe in delString):
            time.sleep(2)
            wline = line
            line = line.strip().split('/')[4]
            print "LINE: " + line
            if uvfits != line:
                print "Wrting " + wline + " to " + STAGE2_PROCESSING_FILE_LIST + "new"
                newfile.write(wline)
    # os.system('mv ' + STAGE2_PROCESSING_FILE_LIST + 'new ' + STAGE2_PROCESSING_FILE_LIST)
    '''
    os.system('sed -i /' + delString + '/d ' + STAGE2_PROCESSING_FILE_LIST)
    time.sleep(5)


def moveProcessedFiles(dest):
    logging.info("Creating " + dest)
    if not os.path.isdir(dest):
        os.makedirs(dest)
    logging.info("Moving all files in fits/* to " + dest)
    os.system('mv ' + curr_dir + 'fits/* ' + dest)


def run_pre_cal_targets():
    os.chdir(curr_dir)
    uvList = glob.glob(BASE_DIR + '*/*.UVFITS')
    assignedList = readFileToList(STAGE2_PROCESSING_FILE_LIST)
    original_stdout = sys.stdout
    original_stderr = sys.stderr
    l2u_precal_log = open('l2u_precal.log', 'a+')
    l2u_precal_log.write('\n\n\n******PRECALIBRATION STARTED******\n\n\n')
    sys.stdout = l2u_precal_log
    sys.stderr = l2u_precal_log
    print len(assignedList)
    while(len(assignedList) != 0):
        for uvfits in uvList:
            uvfits = uvfits.strip()
            if uvfits in assignedList:
                logging.info("Started ParselTounge")
                remove_uvfits_from_assignedList(uvfits)
                projectBaseDir = os.path.dirname(uvfits)
                logging.info("------------------------------------------------------------------")
                logging.info("Running: " + uvfits + " on COMPUTE NODE " + COMPUTE_NODE)
                logging.info("Copying " + uvfits + " to fits/")
                cmd = ['cp', uvfits, 'fits/']
                os.system('cp ' + uvfits + " " + curr_dir + '/fits/')
                UVFITS_FILE = glob.glob("*.UVFITS")
                if UVFITS_FILE != []:
                    # os.chdir('fits')
                    UVFITS_FILE = UVFITS_FILE[0]
                    precalibrator_log = open('precalibrator_log.txt', 'a+')
                    try:
                        logging.info("Running pre_calibrate_targets on " + UVFITS_FILE)
                        spam.pre_calibrate_targets(UVFITS_FILE)
                        precalibrator_log.write(hostname + '\n' + "Successful precalibration of " + UVFITS_FILE + '\n' + 55 * '*' + '\n')
                        precalibrator_log.flush()
                        logging.info("pre_calibrate_targets Success!!!")
                        dest = projectBaseDir + "/PROCESSED/"
                        moveProcessedFiles(dest)
                    except Exception as e:
                        precalibrator_log.write(hostname + '\n' + "Failed precalibration for " + UVFITS_FILE + '\n' + 55 * '*' + '\n')
                        precalibrator_log.flush()
                        dir_name = str(UVFITS_FILE).replace('.', '_')
                        failed_dir = GARUDATA + "/STAGE2_FAILED/" + dir_name + "/"
                        moveProcessedFiles(failed_dir)
                        logging.error("Error: " + str(e))
        assignedList = readFileToList(STAGE2_PROCESSING_FILE_LIST)
    if len(assignedList) == 0:
        logging.info("Done Processing all the UVFITS")
    l2u_precal_log.close()


def __main__():
    run_pre_cal_targets()


if __name__ == "__main__":
    __main__()
