#!/bin/bash
# Launches multiple threads for execution. The ParselTongue start file has been
# edited to support the non-interactive nature of the application.

export SPAM_PATH=/export/spam
export SPAM_HOST=GADPU
export PYTHON=/export/spam/Python-2.7/bin/python
export PYTHONPATH=${SPAM_PATH}/python:${PYTHONPATH}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${SPAM_PATH}/lib
export PATH=${SPAM_PATH}/bin:${PATH}
SRC_DIR="/GARUDATA/LTA/datum_cycle21/Task_Errors/"
cd /home/gadpu/COMPUTE-GADPU;
i=11
while [ $i -lt 18 ]
do
	start_parseltongue.sh THREAD$i/ $i ../$i'exec'.py & 
	i=$[$i+1]
	sleep 20
done

while true
do
	count=`find . -name "THREAD*" -type d | wc -l`
	if [ $count -eq 0 ]; then
		total_files=`ls $SRC_DIR | wc -l`
		valid_logs=`ls $SRC_DIR/*/valid.log | wc -l`
		if [ $valid_logs -eq $total_files ]; then
			sudo mv /etc/rc.local /home/gadpu;
			sleep 60;
			sudo shutdown 0;
		else
			sleep 60;
			sudo reboot;
		fi
	fi
	sleep 60
done
