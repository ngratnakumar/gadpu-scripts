#!/usr/bin/python

import psycopg2
from config import config

CREATE_TABLE_IN_DATABASE = True


def create_tables():
    """ create tables in the PostgreSQL database"""
    commands = (
        """
        CREATE TABLE projects (
            project_id SERIAL PRIMARY KEY,
            proposal_dir VARCHAR(255) NOT NULL,
            base_path VARCHAR(255) NOT NULL,
            observation_num VARCHAR(20) NOT NULL,
            status VARCHAR(20)
        )
        """,
        """
        CREATE TABLE pipeline (
            pipeline_id SERIAL PRIMARY KEY,
            name VARCHAR(30) NOT NULL,
            version INTEGER NOT NULL,
            process_type VARCHAR(30) NOT NULL,
            status VARCHAR(20) NOT NULL,
            started_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        )
        """,
        """
        CREATE TABLE lta_uvfits (
            project_id INTEGER NOT NULL,
            das_scangroup_id INTEGER NOT NULL,
            lta_id SERIAL PRIMARY KEY,
            lta_name VARCHAR(255) NOT NULL,
            status VARCHAR(20),
            process_flag BOOLEAN DEFAULT FALSE,
            started_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            ended_on TIMESTAMP,
            FOREIGN KEY (project_id)
            REFERENCES projects (project_id)
            ON UPDATE CASCADE ON DELETE CASCADE
        )
        """,
        """
        CREATE TABLE calibration(
            calibration_id SERIAL PRIMARY KEY,
            lta_id INTEGER NOT NULL,
            pipeline_id INTEGER NOT NULL,
            uvfits_name VARCHAR(255) NOT NULL,
            status VARCHAR(20),
            uvfits_dir VARCHAR(20),
            process_flag BOOLEAN DEFAULT FALSE,
            started_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            ended_on TIMESTAMP,
            FOREIGN KEY (lta_id)
            REFERENCES lta_uvfits (lta_id)
            ON UPDATE CASCADE ON DELETE CASCADE
        )
        """,
        """
        CREATE TABLE imaging (
            image_id SERIAL PRIMARY KEY,
            pipeline_id INTEGER NOT NULL,
            calibration_id INTEGER NOT NULL,
            fits_name VARCHAR(255) NOT NULL,
            status VARCHAR(20),
            image_dir VARCHAR(20),
            process_flag BOOLEAN DEFAULT FALSE,
            started_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            ended_on TIMESTAMP,
            FOREIGN KEY (calibration_id)
            REFERENCES calibration (calibration_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY (pipeline_id)
            REFERENCES pipeline (pipeline_id)
            ON UPDATE CASCADE ON DELETE CASCADE
        )
        """,
        """
        CREATE TABLE output (
            output_id SERIAL PRIMARY KEY,
            project_id INTEGER NOT NULL,
            pipeline_id INTEGER NOT NULL,
            image_id INTEGER NOT NULL,
            file_name VARCHAR(255) NOT NULL,
            file_type VARCHAR(30) NOT NULL,
            status VARCHAR(20) NOT NULL,
            generated_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY (project_id)
            REFERENCES projects (project_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY (image_id)
            REFERENCES imaging (image_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY (pipeline_id)
            REFERENCES pipeline (pipeline_id)
            ON UPDATE CASCADE ON DELETE CASCADE
        )
        """,
        """
        CREATE TABLE compute_nodes (
            node_id SERIAL PRIMARY KEY,
            node_name VARCHAR(255) NOT NULL,
            threads_count INTEGER DEFAULT 0,
            status VARCHAR(20),
            reboot_flag BOOLEAN DEFAULT FALSE
        )
        """,
        """
        CREATE TABLE compute_threads (
            thread_id SERIAL PRIMARY KEY,
            pipeline_id INTEGER NOT NULL,
            node_id INTEGER,
            thread_dir VARCHAR(255),
            status VARCHAR(20),
            file_name VARCHAR(255) NOT NULL,
            FOREIGN KEY (node_id)
            REFERENCES compute_nodes (node_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY (pipeline_id)
            REFERENCES pipeline (pipeline_id)
            ON UPDATE CASCADE ON DELETE CASCADE
        )
        """)
    conn = None
    try:
        # read the connection parameters
        params = config()
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    if CREATE_TABLE_IN_DATABASE:
        create_tables()
    else:
        print "===> CREATE_TABLE_IN_DATABASE is set to False"
