#!/usr/bin/python

import psycopg2
from config import config

CREATE_TABLE_IN_DATABASE = True


def create_tables():
    """ create tables in the PostgreSQL database"""
    commands = (
        """
        CREATE TABLE projects (
            project_id SERIAL PRIMARY KEY,
            proposal_code VARCHAR(255) NOT NULL,
            proposal_base_path VARCHAR(255) NOT NULL,
            observation_log VARCHAR(20) NOT NULL,
            run_count INTEGER DEFAULT 0,
            cycle_id INTEGER
        )
        """,
        """
        CREATE TABLE lta_uvfits (
            project_id INTEGER NOT NULL,
            lta_id SERIAL PRIMARY KEY,
            lta_name VARCHAR(255) NOT NULL,
            run_count INTEGER DEFAULT 0,
            lta_uvfits_status VARCHAR(20),
            to_be_processed BOOLEAN DEFAULT FALSE,
            lta_status VARCHAR(20),
            FOREIGN KEY (project_id)
            REFERENCES projects (project_id)
            ON UPDATE CASCADE ON DELETE CASCADE
        )
        """,
        """
        CREATE TABLE pre_calibration(
            uvfits_id SERIAL PRIMARY KEY,
            project_id INTEGER NOT NULL,
            lta_id INTEGER NOT NULL,
            uvfits_name VARCHAR(255) NOT NULL,
            pre_calibration_status VARCHAR(20),
            run_count INTEGER DEFAULT 0,
            pre_calibrated_uvfits_dir VARCHAR(20),
            FOREIGN KEY (project_id)
            REFERENCES projects (project_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY (lta_id)
            REFERENCES lta_uvfits (lta_id)
            ON UPDATE CASCADE ON DELETE CASCADE
        )
        """,
        """
        CREATE TABLE process_target (
            fits_id SERIAL PRIMARY KEY,
            project_id INTEGER NOT NULL,
            uvfits_id INTEGER NOT NULL,
            run_count INTEGER DEFAULT 0,
            lta_id INTEGER NOT NULL,
            fits_name VARCHAR(255) NOT NULL,
            process_target_status VARCHAR(20),
            process_target_fits_dir VARCHAR(20),
            FOREIGN KEY (project_id)
            REFERENCES projects (project_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY (lta_id)
            REFERENCES lta_uvfits (lta_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY (uvfits_id)
            REFERENCES pre_calibration (uvfits_id)
            ON UPDATE CASCADE ON DELETE CASCADE
        )
        """,
        """
        CREATE TABLE gadpu_images (
            gadpu_image_id SERIAL PRIMARY KEY,
            project_id INTEGER NOT NULL,
            fits_id INTEGER NOT NULL,
            image_name VARCHAR(255) NOT NULL,
            image_base_path VARCHAR(255) NOT NULL,
            image_status VARCHAR(20) NOT NULL,
            FOREIGN KEY (project_id)
            REFERENCES projects (project_id)
            ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY (fits_id)
            REFERENCES process_target (fits_id)
            ON UPDATE CASCADE ON DELETE CASCADE            
        )
        """,
        """
        CREATE TABLE compute_nodes (
            node_id SERIAL PRIMARY KEY,
            node_name VARCHAR(255) NOT NULL,
            threads_count INTEGER DEFAULT 0,
            status VARCHAR(20),
            reboot_flag BOOLEAN DEFAULT FALSE
        )
        """,
        """
        CREATE TABLE compute_threads (
            thread_id SERIAL PRIMARY KEY,
            node_id INTEGER,
            thread_dir VARCHAR(255),
            status VARCHAR(20),
            stage_name VARCHAR(20),
            file_name VARCHAR(255) NOT NULL,
            FOREIGN KEY (node_id)
            REFERENCES compute_nodes (node_id)
            ON UPDATE CASCADE ON DELETE CASCADE
        )
        """)
    conn = None
    try:
        # read the connection parameters
        params = config()
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    if CREATE_TABLE_IN_DATABASE:
        create_tables()
    else:
        print "===> CREATE_TABLE_IN_DATABASE is set to False"
